import React, {Component} from 'react';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import {Carousel} from 'react-responsive-carousel';
import piggy from './assets/piggy1.JPG';
import palit1 from './assets/palit1.jpg';
import palit2 from './assets/palit2.jpg';
import palit3 from './assets/palit3.jpg';


class DemoCarousel extends Component {
    render() {
        return (
            <div className="home section">
                <Carousel showThumbs={false} 
                showStatus={false} 
                autoPlay 
                wrap
                interval={1000}>
                    <div>
                        <img alt="" src={piggy}/>
                    </div>
                    <div>
                       <img alt="" src={palit1}/>
                    </div>
                    <div>
                       <img alt="" src={palit2}/>
                    </div>
                    <div>
                       <img alt="" src={palit3}/>
                    </div>
                </Carousel>
            </div>
        )
    }
}

export default DemoCarousel