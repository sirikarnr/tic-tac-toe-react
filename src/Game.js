import React from 'react';
import './style/game.css'
import Button from 'react-bootstrap/Button'

/*There are 3 React Components in this js file (Square, Board and Game) */

/*Square component render a single button */
/*Board component renders 9 squares */
/*Game component renders a board with placeholder */

//Function Component change from SquareReactComponent class
function Square(props) {
    return (
        /*
        onClick={() => this.props.onClick()} 
        to a shorter
        onClick={props.onClick}
        */
        <Button className="square"  onClick={props.onClick}>
            {props.value}
        </Button>
    )
}

/*
class SquareReactComponent extends React.Component {
    //Tell component to rember things use "state"
    //constructor to initialize the state
    //comment this constructor because it's no longet keeps track of the game's state
    constructor(props) {
        //All React component classes that have "constructor" should start with "super(props)" call
        super(props); //In js classes, need to always call "super" when defining the constructor of a subclass
        this.state = {
            value: null,
        };
    }

    //Square is child component
    render() {
        return (
            <button className="square" 
                    onClick={() => this.props.onClick()} 
            >
                {this.props.value}
            </button>
        );
    }
}
*/

class Board extends React.Component {
    //Board  is parent component
    //Passing data thrugh props
    renderSquare(i){
        //this code is to pass a prop called value to the Square component
        return (
                <Square 
                    value={this.props.squares[i]}
                    onClick={() => this.props.onClick(i)}
                />                
        );
    }

    render(){
        return (
            <div>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
            
        );
    }
}

function calculateTheWinner(squares) {
    const lines = [
        [0,1,2],
        [3,4,5],
        [6,7,8],
        [0,3,6],
        [1,4,7],
        [2,5,8],
        [0,4,8],
        [2,4,6]
    ];
    for (let i=0; i < lines.length; i++){
        const [a,b,c] = lines[i];
        if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]){
            return squares[a]
        }
    }
    return null;
}

class Game extends React.Component {   
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null)
            }],
            stepNumber: 0,
            xIsNext: true
        };
    } 
    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length -1];
        const squares = current.squares.slice();
        if(calculateTheWinner(squares) || squares[i]){
            return;
        }
        //.slice() operator used to create a copy of the squares array to modify instead of modifying the existing array (immutability)
        squares[i] =  this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
        });
    }

    jumpTo(step) {       
            this.setState({
                stepNumber: step,
                xIsNext: (step%2) === 0,
            });
    }

    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = calculateTheWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move ?
                'Go to move #' + move :
                'Go to game start';
            return (
                <li key={move}>
                    <button className="state" onClick={() => this.jumpTo(move)}>{desc}</button>
                </li>
            );
        });

        let status;
        if (winner) {
            status = 'Game Over! The winner is ' + (winner==='X'? 'Player 1' : 'Player 2');
        } else{
            status = 'Next player: ' + (this.state.xIsNext ? 'Player 1 (X)' : 'Player 2 (O)');
        }

        return (
            <div className="game section" id="game">
                <div className="game-board">
                <Board 
                    squares={current.squares}
                    onClick={(i) => this.handleClick(i)}
                />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
                <div>
                    
                </div>
            </div>
        );
    }
}

export default Game;
