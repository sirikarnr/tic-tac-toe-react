import React, {Component} from 'react';
import './style/product.css'; 
import Button from 'react-bootstrap/Button';

import img1 from './image/1.jpg';
import cat from './image/cat.jpg';

function ProdContent(props) {
    return ( 
        <div className="product-box" style={{backgroundColor: props.bg}}>
            <div className="row">
                <div className="col">
                    <img src={props.img} alt={props.img} />
                </div>
            </div>
            <div className="row">
                <div className="col product-box-item title">
                    {props.title}
                </div>
            </div>
            <div className="row">
                <div className="col product-box-item blurb">
                    {props.details}
                </div>
            </div>
            <div className="row">
                <div className="col" >
                    <Button className="product-box-item button" >
                        Learn more 
                    </Button>
                </div>
            </div>
        </div>      
    );
}

class Product extends Component {
    render() {
        return (
            <div className="product section" id="product">        
                <h2>Explore Product</h2>
                    <ProdContent title="Hedgehog" img={img1} bg="rgba(226,220,124,1)"
                    details="A hedgehog is any of the spiny mammals of the subfamily Erinaceinae, 
                    in the eulipotyphlan family Erinaceidae. "/>

                    <ProdContent title="Cat" img={cat} bg="rgba(163,205,57,1)"
                    details="The cat or domestic cat (Felis catus) is a small carnivorous mammal.[1][2] It is the only domesticated species in the family Felidae. "/>
            </div>
        )
    }
}

export default Product