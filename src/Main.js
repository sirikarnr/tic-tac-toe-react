import React, {Component} from "react";
import {
    NavLink,
    BrowserRouter, 
    //Route, 
    //Switch
} from "react-router-dom";
// import Home from "./Home";
// import Game from "./Game";
import ScrollIntoView from 'react-scroll-into-view'


class Main extends Component {

    render() {
        return (
            <BrowserRouter>
            <div>
                <ul className="header">
                    <li>
                        <NavLink className="nav" exact to="/">
                            <ScrollIntoView selector="#home">Home</ScrollIntoView>
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to="/game">
                            <ScrollIntoView selector="#game">Tic Tac Toe</ScrollIntoView>
                        </NavLink>
                    </li>
                </ul>
                {/* <h1>Simple React App</h1>
                <div className="content">
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route path="/game" component={Game}/> 
                </Switch>
                </div> */}
               
            </div>
            </BrowserRouter>
        );
    }
}

export default Main;