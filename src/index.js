import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min';
import React from 'react';
import ReactDOM from 'react-dom';
import './style/index.css';
import Home from './Home';
import Game from './Game';
import Menu from './Menu';
import Contact from './Contact';
import Product from './Product';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fab } from '@fortawesome/free-brands-svg-icons'
import { faFeather, faPhone, faComments, faEnvelope } from '@fortawesome/free-solid-svg-icons'
import DemoCarousel from './Slide';
 
library.add(fab, faEnvelope, faFeather, faPhone, faComments)

const App = () => (
    <div>
        <Menu/>
        <DemoCarousel/>
        <Home/>
        <Game/>
        <Product/>
        <Contact/>
    </div>
);

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);
